# Scott Punshon - Hometrack Code Test Submission

## Running API

Start the API on default port (8000) by running:

```
./bin/api
```


## Hosting

Hosted on Heroku at <https://vast-harbor-86754.herokuapp.com/>


## Improvements

#### Tests

The test code is literally just a set of shell scripts.

If I had more time (and wasn't going away at this moment for the weekend) I would have written proper unit tests (in JS) for my filter function and transformation function.


## Additional Comments

In a manual deployment, I would probably put a proxy in between external internet and the Node.js app - such as Nginx. This improves security, and also allows better caching and faster delivery of static files (Nginx's speciality :P).
I'm sure Heroku handles these sort of things for you, but I personally don't use Heroku so unsure - and this would be considerations if unable (for whatever reason) to leverage such hosting services.

My experience with HA configurations is quite manual - configurations involved multiple instances with block level replication using DRBD. Heartbeat is then set up between the nodes on 2nd network interfaces, and a virtual IP address managed between them (using the heartbeat) to achieve High Availability.
Again, I know AWS and other similar services support these sort of HA setups easily across different availability zones, but my experience is on a more manual level and unfortunately I don't have much spare time this weekend to read up on how to do it on Heroku.
