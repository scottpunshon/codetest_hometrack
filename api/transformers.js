/**
 * Author: Scott Punshon
 *
 * Module data transformers. Accept a single data entry
 * and modify its structure in a defined way.
 */


/*
 * Takes a single property entry and returns the summarised version of it.
 */
function property_summary(property) {
	const addr_keys = ['unitNumber', 'buildingNumber', 'street', 'suburb', 'state', 'postcode'];
	const addr_sep = ' ';
	//const addr_sep = ', ';  // support more complex/better separators if needed :)

	var addr = property.address;

	var concataddr = '';  // start with blank string, incase fields are integers they will cast to string
	for (var i = 0; i < addr_keys.length; i++) {
		var key = addr_keys[i];
		if (addr[key]) {  // don't join in blank/undefined/null field values (they might occur)
			concataddr += addr[key] + addr_sep;
		}
	}
	concataddr = concataddr.slice(0, -addr_sep.length);  // handle that last trailing address separator

	// Our summarised property structure
	var summary = {
		'workflow': property.workflow,
		'type': property.type,
		'concataddress': concataddr,
	};

	return summary;
}


// Exposed data transformers
module.exports = {
	'property_summary': property_summary
};
