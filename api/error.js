/**
 * Author: Scott Punshon
 *
 * Module for everything related to errors interfacing with the API.
 */


/*
 * As per specification:
 * Processes and returns a consistent payload for all errors interfacing with the API.
 */
function error() {
	return {
		'error': 'Could not decode request: JSON parsing failed'
	};
}


module.exports = error;
