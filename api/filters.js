/**
 * Author: Scott Punshon
 *
 * Module containing API data filters
 */


/*
 * Filters an array of property entries, returning only those which have:
 *   type = htv
 *   workflow = completed
 */
function completed_htv_properties(properties_arr) {
	// Always return consistent data structure (an array), regardless
	// of if the input data is not good (e.g. not an array).
	var filtered = [];

	if (typeof properties_arr.filter === 'function') {
		filtered = properties_arr.filter((prop) => prop.workflow === 'completed' && prop.type === 'htv');
	}
	return filtered;
}


// Exposed data filters
module.exports = {
	'completed_htv_properties': completed_htv_properties
};
