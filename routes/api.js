/**
 * Author: Scott Punshon
 *
 * API routes.
 */
var express = require('express');
var router = express.Router();

var api_error = require('../api/error');
var api_filter = require('../api/filters');
var api_transformer = require('../api/transformers');


/*
 * Lets screen all incoming Content-Types - if it isn't flagged
 * as a JSON payload, we want nothing to do with it...
 */
router.use('/', function(req, res, next) {
	var content_type = req.headers['content-type'];
	if (!content_type || content_type.indexOf('application/json') !== 0) {
		return res.status(400).json(api_error());
	}
	next();
});


/*
 * Route for POST requests.
 * We are expecting to be a JSON payload of properties under the "payload" key.
 * Return a response JSON object based on incoming payload per the specification.
 */
router.post('/', function(req, res, next) {
	// JSON body has been parsed by middleware into a JS object
	var in_json = req.body;

	// Expect an array of property objects under the 'payload' key.
	// NOTE: no need to check payload key exists - it will error out further
	// down and our "catch-all" error handler will take care of sending the error.
	var properties = in_json.payload;

	// Our base JSON response template
	var out_json = {'response': []};

	// Filter the properties list
	var filtered = api_filter.completed_htv_properties(properties);
	// Transform the remaining property objects
	for (var i = 0; i < filtered.length; i++) {
		out_json.response.push(
			api_transformer.property_summary(filtered[i])
		);
	}

	// Done - send the JSON response
	res.json(out_json);
});


/*
 * Route for GET requests.
 * The specification only mentioned "We'll post some JSON data", and didn't
 * mention how to handle invalid HTTP verbs - so will leave them unhandled
 * (and hence default to error handler)
 */
/*
router.get('/', function(req, res, next) {
	// DEBUG: Send something anyway for now, for browsers who stumble here
	res.send('*POST* your valid JSON - thanks');
});
*/


module.exports = router;
