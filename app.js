/**
 * Author: Scott Punshon
 *
 * Main entry point for application.
 */
var express = require('express');
var logger = require('morgan');
var bodyParser = require('body-parser');

// Routes
var api = require('./routes/api');

var app = express();


// Setup logging
app.use(logger('dev'));

// This is really the only middleware we need, seeing as this is a pure JSON API
// i.e. no XML, no HTML/views, no favicons, no user sessions, etc
app.use(bodyParser.json());

// As per specification:
// The API base is to be anchored to the root URL.
app.use('/', api);


// Catch-all Error handler
const api_error = require('./api/error');

// Catch all/fallthrough route. We just create an error object for
// good measure and psh it on.
app.use(function(req, res, next) {
	// As per specification:
	// All unhandled scenarios are HTTP 400 errors
	var err = new Error('Bad Request');
	err.status = 400;
	next(err);
});

// Error handler function
app.use(function(err, req, res, next) {
	// Set the status from the error and send our specific error JSON payload.
	res.status(400).json(api_error());
});


module.exports = app;
